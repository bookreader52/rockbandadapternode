Firmware files for the test version of the Wii nunchuck guitar adapter. 
<br><br>
To download a file, click the file, and then click View Raw, and the file should download. For instructions on updating the adapter, see (https://rolllimitless.com/firmwares/)
<br><br>
Files are named ThresholdXFramesY.uf2 . Threshold8Frames6.uf2 is the default firmware. If a firmware contains "slider" at the end of the name, the slider bar on GH4/5 guitars will work with this firmware.
<br>
<ul>
<li>The treshold number (X) is the amount you have to tilt the guitar, lower numbers mean you have to tilt the guitar more.</li>
<li>The Frames number (Y) is how many frames you have to tilt the guitar before the guitar activates (Assumes 60FPS, or 16ms per frame). Fast Alt-Strumming can temporarily cause the guitar to be read as tilted. Higher Frame numbers will help stop false activations from fast Alt-Strumming, but will also add a slight delay before overdrive activates</li>
</ul>