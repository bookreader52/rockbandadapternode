var app = angular.module('rockBandApp', []);
app.controller('rockBandController', function($scope) {
    $scope.minDelayPresets = {
        RB4: {
            m1 : 12,
            m2 : 24,
            m3 : 12
        }, 
        RB3: {
            m1: 10,
            m2: 25,
            m3: 25
        }
    }
    $scope.drumNoteInfo = [
        {name:"Red Pad", notes:"31, 34, 37, 38, 39, 40"},
        {name:"Yellow Pad", notes:"48, 50"},
        {name:"Blue Pad", notes:"45, 47"},
        {name:"Green Pad", notes:"41, 43"},
        {name:"Yellow Cymbal", notes:"22, 26, 42, 46, 54"},
        {name:"Blue Cymbal", notes:"51, 53, 56, 59"},
        {name:"Green Cymbal", notes:"49, 52, 55, 57"},
        {name:"Kick", notes:"33, 35, 36"},
        {name:"Double Kick", notes:"44"}
    ]
    $scope.controllerState = [0,[],[],[]];
    $scope.lag = 0;

    $scope.socket = io();

    $scope.previousNotes = [];
    $scope.maxPreviousNotesLength = 100;

    $scope.currentPage = "";
    $scope.currentMainPage = "";

    $scope.socket.on("Version", function(version){
        $scope.version = version;
        $scope.$apply();
    })

    $scope.socket.on("Devices", function(devices){
        $scope.devices={devices:devices};
        $scope.selectedDevice = $scope.devices.devices[0];
        $scope.$apply();
    })

    $scope.socket.on("Device Info", function(deviceInfo){
        $scope.deviceInfo = deviceInfo;
        if(!deviceInfo.connected) {
            $scope.controllerState = [0,[],[],[]];
            $scope.currentPage = "";
            $scope.previousNotes = [];
        }
        $scope.$apply();
    })

    $scope.socket.on("Controller State", function(controllerState){
        //console.log(controllerState);
        $scope.controllerState = controllerState;
        if($scope.controllerState[0]==0) {
            $scope.delay = $scope.controllerState[5]||0;
            $scope.m1 = $scope.controllerState[8]||0;
            $scope.originalDelay = $scope.delay;
            $scope.originalM1 = $scope.m1;
        } else if($scope.controllerState[0]==1) {
            $scope.threshold = $scope.controllerState[5]||0;
            $scope.originalThreshold = $scope.threshold;
            if($scope.controllerState[6] && $scope.controllerState[7]) {
                $scope.previousNotes.push({note:$scope.controllerState[6], sensitivity:$scope.controllerState[7], rbNote:{red:!!controllerState[2][2], yellow:!!controllerState[2][3], blue:!!controllerState[2][0], green:!!controllerState[2][1], kick:!!controllerState[2][4], doubleKick:!!controllerState[2][5], pad:!!controllerState[3][2], cymbal:!!controllerState[3][3], error:controllerState[8]}});
                if($scope.previousNotes.length > $scope.maxPreviousNotesLength) {
                    $scope.previousNotes.splice(0,$scope.previousNotes.length-$scope.maxPreviousNotesLength);
                }

                if($scope.midiToRecord) {
                    $scope.socket.emit("Change MIDI",69+$scope.midiToRecord,$scope.controllerState[6]);
                    $scope.cancelRecord();
                }
            }
            $scope.oldMidiNumbers = [];
            for(var i=0;i<9;i++) {
                $scope.oldMidiNumbers.push($scope.controllerState[9+i]);
            }
            $scope.mil1 = $scope.controllerState[18];
            $scope.mil2 = $scope.controllerState[19];
            $scope.mil3 = $scope.controllerState[20];
            $scope.originalMil1 = $scope.mil1;
            $scope.originalMil2 = $scope.mil2;
            $scope.originalMil3 = $scope.mil3;
            $scope.everyNoteDifferentPacket = $scope.controllerState[21];
        }
        $scope.$apply();
    })

    $scope.socket.on("error", function(error){
        alert(error);
    })

    $scope.connectToDevice = function(n, a) {
        $scope.socket.emit("Connect To Device", n, a);
    }

    $scope.disconnectFromDevice = function() {
        $scope.socket.emit("Disconnect From Device", $scope.selectedDevice.name, $scope.selectedDevice.address);
    }

    $scope.refreshDeviceList = function() {
        $scope.socket.emit("Refresh Device List");
    }

    $scope.btText = "hi";
    $scope.sendText = function() {
        $scope.socket.emit("Send String", $scope.btText);
    }

    $scope.changeTilt = function() {
        $scope.socket.emit("Change Tilt",(!$scope.controllerState[6]?1:0));
    }

    $scope.changeDelay = function() {
        $scope.socket.emit("Change Delay",$scope.delay);
    }

    $scope.changeM1 = function() {
        $scope.socket.emit("Change M1",$scope.m1);
    }

    $scope.changeThreshold = function() {
        $scope.socket.emit("Change Threshold",$scope.threshold);
    }

    $scope.changeMIDI = function(m) {
        $scope.socket.emit("Change MIDI",70+m,$scope.controllerState[9+m]);
    }

    $scope.changeMil1 = function() {
        $scope.socket.emit("Change Mil1",$scope.mil1);
    }

    $scope.changeMil2 = function() {
        $scope.socket.emit("Change Mil2",$scope.mil2);
    }

    $scope.changeMil3 = function() {
        $scope.socket.emit("Change Mil3",$scope.mil3);
    }

    $scope.changeEveryNoteDifferentPacket = function(){
        if($scope.everyNoteDifferentPacket==69) {
            $scope.socket.emit("Change everyNoteDifferentPacket",70);
        } else {
            $scope.socket.emit("Change everyNoteDifferentPacket",69);
        }
    }

    $scope.changeMinDelayPreset = function(r) {
        $scope.socket.emit("Change Mil1",$scope.minDelayPresets[r].m1);
        $scope.socket.emit("Change Mil2",$scope.minDelayPresets[r].m2);
        $scope.socket.emit("Change Mil3",$scope.minDelayPresets[r].m3);
    }

    $scope.switchToPage = function(p) {
        $scope.currentPage = p;
    }

    $scope.switchToMainPage = function(p) {
        $scope.currentMainPage = p;
    }

    $scope.update = function(u) {
        $scope.socket.emit("Update",u);
    }

    $scope.recordMIDI = function(m, s) {
        $scope.midiToRecord = m+1;
        $scope.midiToRecordString = s;
    }

    $scope.resetMIDI = function(m) {
        $scope.socket.emit("Change MIDI",70+m,0);
    }

    $scope.resetAllMIDI = function(m) {
        for(var i = 0;i<9;i++) {
            $scope.socket.emit("Change MIDI",70+i,0);
        }
    }

    $scope.cancelRecord = function() {
        $scope.midiToRecord = false;
        $scope.midiToRecordString = false;
    }

    $scope.resetPreviousNotes = function() {
        $scope.previousNotes = [];
    }

    $scope.rbNoteToString = function(rbNote) {
        var s = "";
        if(rbNote.red) {
            s+="Red ";
        } else if(rbNote.yellow) {
            s+="Yellow ";
        } else if(rbNote.blue) {
            s+="Blue ";
        } else if(rbNote.green) {
            s+="Green ";
        }

        if(rbNote.pad) {
            s+="pad";
        } else if(rbNote.cymbal) {
            s+="cymbal";
        }

        if(rbNote.kick) {
            s="Kick";
        } else if(rbNote.doubleKick) {
            s="Double Kick";
        }

        if(rbNote.error==1){
            s="Threshold too high";
        }

        if(rbNote.error==2){
            s="Invalid MIDI Note";
        }
        
        if(s=="") {
            s="N/A";
        }
        return s;

    }
});