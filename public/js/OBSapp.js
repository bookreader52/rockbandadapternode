socket = io();

var timeBeforeEndingNote = 40;
var drumTimeouts = [];
for(var i=0;i<9;i++) {
    drumTimeouts.push("false");
}

var green = document.getElementById("green");
var red = document.getElementById("red");
var yellow = document.getElementById("yellow");
var blue = document.getElementById("blue");
var orange = document.getElementById("orange");

var redPad = document.getElementById("redPad");
var yellowPad = document.getElementById("yellowPad");
var bluePad = document.getElementById("bluePad");
var greenPad = document.getElementById("greenPad");
var yellowCymbal = document.getElementById("yellowCymbal");
var blueCymbal = document.getElementById("blueCymbal");
var greenCymbal = document.getElementById("greenCymbal");
var kick = document.getElementById("kick");
var doubleKick = document.getElementById("doubleKick");

var up = document.getElementById("up");
var down = document.getElementById("down");

document.getElementById("guitarDiv").style.height=green.clientWidth;

socket.on("Device Info", function(deviceInfo){
    redPad.src="/img/red.png";
})

socket.on("Controller State", function(controllerState){
    if(controllerState[0]==0) {
        //guitar

        document.getElementById("guitarDiv").style.display="block";
        document.getElementById("drumsDiv").style.display="none";

        if(controllerState[2][1]) {
            green.src="/img/greenFill.png";
        } else {
            green.src="/img/green.png"
        }

        if(controllerState[2][2]) {
            red.src="/img/redFill.png";
        } else {
            red.src="/img/red.png"
        }

        if(controllerState[2][3]) {
            yellow.src="/img/yellowFill.png";
        } else {
            yellow.src="/img/yellow.png"
        }

        if(controllerState[2][0]) {
            blue.src="/img/blueFill.png";
        } else {
            blue.src="/img/blue.png"
        }

        if(controllerState[2][4]) {
            orange.src="/img/orangeFill.png";
        } else {
            orange.src="/img/orange.png"
        }

        if(controllerState[4][0]) {
            up.src="/img/grayFill.png";
        } else {
            up.src="/img/gray.png"
        }

        if(controllerState[4][2]) {
            down.src="/img/grayFill.png";
        } else {
            down.src="/img/gray.png"
        }
    } else if(controllerState[0]==1) {
        //drums

        document.getElementById("guitarDiv").style.display="none";
        document.getElementById("drumsDiv").style.display="block";

        if(controllerState[2][2] && controllerState[3][2]) {
            redPad.src="/img/redFill.png";
            clearTimeout(drumTimeouts[0]);
            drumTimeouts[0] = setTimeout(function(){
                redPad.src="/img/red.png";
                drumTimeouts[0] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][3] && controllerState[3][2]) {
            yellowPad.src="/img/yellowFill.png";
            clearTimeout(drumTimeouts[1]);
            drumTimeouts[1] = setTimeout(function(){
                yellowPad.src="/img/yellow.png";
                drumTimeouts[1] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][0] && controllerState[3][2]) {
            bluePad.src="/img/blueFill.png";
            clearTimeout(drumTimeouts[2]);
            drumTimeouts[2] = setTimeout(function(){
                bluePad.src="/img/blue.png";
                drumTimeouts[2] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][1] && controllerState[3][2]) {
            greenPad.src="/img/greenFill.png";
            clearTimeout(drumTimeouts[3]);
            drumTimeouts[3] = setTimeout(function(){
                greenPad.src="/img/green.png";
                drumTimeouts[3] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][3] && controllerState[3][3]) {
            yellowCymbal.src="/img/yellowFill.png";
            clearTimeout(drumTimeouts[4]);
            drumTimeouts[4] = setTimeout(function(){
                yellowCymbal.src="/img/yellow.png";
                drumTimeouts[4] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][0] && controllerState[3][3]) {
            blueCymbal.src="/img/blueFill.png";
            clearTimeout(drumTimeouts[5]);
            drumTimeouts[5] = setTimeout(function(){
                blueCymbal.src="/img/blue.png";
                drumTimeouts[5] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][1] && controllerState[3][3]) {
            greenCymbal.src="/img/greenFill.png";
            clearTimeout(drumTimeouts[6]);
            drumTimeouts[6] = setTimeout(function(){
                greenCymbal.src="/img/green.png";
                drumTimeouts[6] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][4] && controllerState[3][2]) {
            bass.src="/img/orangeFill.png";
            clearTimeout(drumTimeouts[7]);
            drumTimeouts[7] = setTimeout(function(){
                bass.src="/img/orange.png";
                drumTimeouts[7] = false;
            }, timeBeforeEndingNote)
        }

        if(controllerState[2][5] && controllerState[3][2]) {
            doubleBass.src="/img/orangeFill.png";
            clearTimeout(drumTimeouts[8]);
            drumTimeouts[8] = setTimeout(function(){
                doubleBass.src="/img/orange.png";
                drumTimeouts[8] = false;
            }, timeBeforeEndingNote)
        }
    }
})