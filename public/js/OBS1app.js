socket = io();

var colors = ["green","red","yellow","blue","orange"]

var divisor = 3;

var guitarDiv = document.getElementById("guitarDiv");
var drumsDiv = document.getElementById("drumsDiv");

var upStrum = false;
var downStrum = false;

var controllerPackets = [];

var notes=[];

var lastPacket = [false, false, false, false, false, false];

var guitar = false;
var drums = false;

socket.on("Controller State", function(controllerState){
    if(controllerState[0]==0) {
        guitar = true;
        drums = false;
        guitarDiv.style.display="block";
        drumsDiv.style.display="none";

        var thisPacket = [new Date().getTime(),false,false,false,false,false,false];
        //guitar

        if(controllerState[2][1]) {
            thisPacket[1]=true;
        }

        if(controllerState[2][2]) {
            thisPacket[2]=true;
        }

        if(controllerState[2][3]) {
            thisPacket[3]=true;
        }

        if(controllerState[2][0]) {
            thisPacket[4]=true;
        }

        if(controllerState[2][4]) {
            thisPacket[5]=true;
        }

        if(controllerState[4][0]) {
            if(!upStrum) {
                upStrum=true;
                thisPacket[6]=true;
            }
            downStrum = false;
        }

        if(controllerState[4][2]) {
            if(!downStrum) {
                downStrum=true;
                thisPacket[6]=true;
            }
            upStrum = false;
        }
        
        if(!controllerState[4][0] && !controllerState[4][2]) {
            upStrum = false;
            downStrum = false;
        }

        var bb=true;
        for(var i=0;i<5;i++) {
            if(thisPacket[i+1]!=lastPacket[i]) {
                bb=false;
            }
        }
        if(!bb || thisPacket[6]) {
            controllerPackets.push(thisPacket);
        }

        for(var i=0;i<6;i++) {
            lastPacket[i]=thisPacket[i+1];
        }
    } else if(controllerState[0]==1) {
        drums=true;
        guitar=false;
        drumsDiv.style.display="block";
        guitarDiv.style.display="none";
        
        var thisPacket = [new Date().getTime(),false,false,false,false,false,false,false];
        //drums

        if(controllerState[2][1]) {
            thisPacket[1]=true;
        }

        if(controllerState[2][2]) {
            thisPacket[2]=true;
        }

        if(controllerState[2][3]) {
            thisPacket[3]=true;
        }

        if(controllerState[2][0]) {
            thisPacket[4]=true;
        }

        if(controllerState[2][4] || controllerState[2][5]) {
            thisPacket[5]=true;
        }

        if(controllerState[3][3]) {
            thisPacket[6]=true;
        }

        if(controllerState[3][2]) {
            thisPacket[7]=true;
        }
        
        controllerPackets.push(thisPacket);
    }
})

function draw() {
    if(guitar) {
        for(var i=0;controllerPackets[i] && controllerPackets[i][0]<new Date().getTime()-1000;i++){
            for(var ii=0;ii<colors.length;ii++) {
                if(notes[controllerPackets[i][0]+colors[ii]]) {
                    guitarDiv.removeChild(notes[controllerPackets[i][0]+colors[ii]]);
                    delete notes[controllerPackets[i][0]+colors[ii]];
                }
                if(notes[controllerPackets[i][0]+colors[ii]+"HOPO"]) {
                    guitarDiv.removeChild(notes[controllerPackets[i][0]+colors[ii]+"HOPO"]);
                    delete notes[controllerPackets[i][0]+colors[ii]+"HOPO"];
                }
            }
            controllerPackets.shift();
            i--;
        }
        for(var i=0;i<controllerPackets.length;i++){
            var d = (new Date().getTime()-controllerPackets[i][0])
            for(var ii=1;ii<=5;ii++) {
                if(controllerPackets[i][ii]) {
                    if(notes[controllerPackets[i][0]+colors[ii-1]]) {
                        notes[controllerPackets[i][0]+colors[ii-1]].style.top = d/divisor;
                    } else {
                        notes[controllerPackets[i][0]+colors[ii-1]] = document.createElement("div");
                        if(!controllerPackets[i][6]) {
                            notes[controllerPackets[i][0]+colors[ii-1]].classList.add(colors[ii-1]+"HOPO")
                        } else {
                            notes[controllerPackets[i][0]+colors[ii-1]].classList.add(colors[ii-1])
                        }
                        notes[controllerPackets[i][0]+colors[ii-1]].style.top = d/divisor;
                        notes[controllerPackets[i][0]+colors[ii-1]].style.left = 40*(ii-1);
                        guitarDiv.appendChild(notes[controllerPackets[i][0]+colors[ii-1]]);
                    }
                }
            }
        }
    } else if(drums) {
        for(var i=0;controllerPackets[i] && controllerPackets[i][0]<new Date().getTime()-1000;i++){
            for(var ii=0;ii<colors.length;ii++) {
                if(notes[controllerPackets[i][0]+colors[ii]]) {
                    drumsDiv.removeChild(notes[controllerPackets[i][0]+colors[ii]]);
                    delete notes[controllerPackets[i][0]+colors[ii]];
                }
                if(notes[controllerPackets[i][0]+colors[ii]+"Cymbal"]) {
                    drumsDiv.removeChild(notes[controllerPackets[i][0]+colors[ii]+"Cymbal"]);
                    delete notes[controllerPackets[i][0]+colors[ii]+"Cymbal"];
                }
            }
            controllerPackets.shift();
            i--;
        }
        for(var i=0;i<controllerPackets.length;i++){
            var d = (new Date().getTime()-controllerPackets[i][0])
            for(var ii=1;ii<=5;ii++) {
                if(controllerPackets[i][ii]) {
                    if(notes[controllerPackets[i][0]+colors[ii-1]]) {
                        notes[controllerPackets[i][0]+colors[ii-1]].style.top = d/divisor;
                    } else {
                        notes[controllerPackets[i][0]+colors[ii-1]] = document.createElement("div");
                        if(controllerPackets[i][6]) {
                            notes[controllerPackets[i][0]+colors[ii-1]].classList.add(colors[ii-1]+"Cymbal");
                            if(colors[ii-1]=="green") {
                                notes[controllerPackets[i][0]+colors[ii-1]].style.left = 120;
                            } else {
                                notes[controllerPackets[i][0]+colors[ii-1]].style.left = 40*(ii-2);
                            }
                        } else if (controllerPackets[i][7]) {
                            if(colors[ii-1]=="orange") {
                                notes[controllerPackets[i][0]+colors[ii-1]].classList.add("kick");
                                notes[controllerPackets[i][0]+colors[ii-1]].style.left = 0;
                            } else if(colors[ii-1]=="green") {
                                notes[controllerPackets[i][0]+colors[ii-1]].classList.add("green");
                                notes[controllerPackets[i][0]+colors[ii-1]].style.left = 120;
                            } else {
                                notes[controllerPackets[i][0]+colors[ii-1]].classList.add(colors[ii-1]);
                                notes[controllerPackets[i][0]+colors[ii-1]].style.left = 40*(ii-2);
                            }
                        }
                        notes[controllerPackets[i][0]+colors[ii-1]].style.top = d/divisor;
                        drumsDiv.appendChild(notes[controllerPackets[i][0]+colors[ii-1]]);
                    }
                }
            }
        }
    }
    setTimeout(draw, 15);
}

draw();