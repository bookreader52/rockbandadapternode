var fs = require('fs');
var child_process = require('child_process');
var deviceInfo = {connected:false, name:""};
var numberOfConnects = 0;
var possibleNames = ["Rock Band Adapter"];
var listOfDevices = [];
var btSerial;
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static('public'));
app.get('/', function(req, res) {
    res.sendFile("public/index.html");
})

var b = fs.readFileSync("./version.json")
var version=JSON.parse(b);
console.log("Version: " + version.version);

var http = require('http').Server(app);

var io = require('socket.io')(http);

var bits = [[],[],[],0,0,0];

http.listen(3000, '127.0.0.1', function() {
    console.log("Go to http://localhost:3000 in a web browser");
})

io.on('connection', function(socket){
    numberOfConnects++;
    //console.log('an user connected');
    socket.emit('Version', version);
    socket.emit('Devices', listOfDevices);
    socket.emit("Device Info", deviceInfo);
    io.emit("Controller State",bits);

    socket.on('Refresh Device List', function(){
        updateDeviceList();
    });

    socket.on('Connect To Device', function(name, address){
        if(!deviceInfo.connected) connectToDevice(name, address);
    });

    socket.on('Disconnect From Device', function(){
        disconnectFromDevice();
    })

    socket.on('Send String', function(s){
        sendString(s);
    })

    socket.on('Change Tilt', function(t){
        sendString([169,t]);
    })

    socket.on('Change Delay', function(l){
        sendString([69,l]);
    })

    socket.on('Change M1', function(m){
        sendString([20,m]);
    })

    socket.on('Change Threshold', function(t){
        sendString([42,t]);
    })

    socket.on('Change MIDI', function(m,mm){
        sendString([m,mm]);
    })

    socket.on('Change Mil1', function(m){
        sendString([10,m]);
    })

    socket.on('Change Mil2', function(m){
        sendString([11,m]);
    })

    socket.on('Change Mil3', function(m){
        sendString([12,m]);
    })

    socket.on('Change everyNoteDifferentPacket', function(m){
        sendString([25,m]);
    })

    socket.on('Update', function(whatToUpdate){
        child_process.exec('update'+whatToUpdate+'.bat', function(error, stdout, stderr) {
            console.log(stdout);
        });
    })

    socket.on('disconnect', function(){
        numberOfConnects--;
        /*if(numberOfConnects==0) {
            disconnectFromDevice();
        }*/
    });
});

function getBit(byte, position) {
    try {
    return (byte & (1 << position)) != 0;
    } catch(e) {
        console.log("---ERROR--");
        console.log(byte, position);
        console.log("--END ERROR---");
        return 0;
    }
}

var connectToDevice;
var updateDeviceList;
var disconnectFromDevice;
var sendString;

function parseMessage(buffer) {
    var newBuffer = [];
    if(buffer.length > 4) {
        if(buffer[0] === 0) {
            //guitar
            bits = [buffer[0],buffer[1],[],[],[]];
            for(var i=0;i<8;i++) {
                bits[2].push(getBit(buffer[2],i));
            }
            for(var i=0;i<8;i++) {
                bits[3].push(getBit(buffer[3],i));
            }
            for(var i=0;i<8;i+=2) {
                bits[4].push(buffer[4]==i);
            }
            bits.push(buffer[5]);
            bits.push(buffer[6]);
            bits.push(buffer[7]);
            bits.push(buffer[8]);
            if(buffer.length>9) {
                for(var i=9;i<buffer.length;i++) {
                    newBuffer.push(buffer[i]);
                }
            }
        } else if(buffer[0] === 1) {
            //guitar
            bits = [buffer[0],buffer[1],[],[],[]];
            for(var i=0;i<8;i++) {
                bits[2].push(getBit(buffer[2],i));
            }
            for(var i=0;i<8;i++) {
                bits[3].push(getBit(buffer[3],i));
            }
            for(var i=0;i<8;i+=2) {
                bits[4].push(buffer[4]==i);
            }
            for(var i=0;i<13;i++) {
                bits.push(buffer[5+i]);
            }
            bits.push(buffer[18]);
            bits.push(buffer[19]);
            bits.push(buffer[20]);
            bits.push(buffer[21]);
            if(buffer.length>22) {
                for(var i=22;i<buffer.length;i++) {
                    newBuffer.push(buffer[i]);
                }
            }
        }
        if(buffer[0] === 0 || buffer[0] === 1) {
            io.emit("Controller State",bits);
        }
        if(newBuffer.length>4) {
            parseMessage(newBuffer);
        }
    }
}

function setup() {
    var btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort();

    disconnectFromDevice = function() {
        btSerial.close();
    }

    updateDeviceList = function() {
        listOfDevices=[];
        io.emit("Devices", listOfDevices);
        btSerial.inquire();
    }

    connectToDevice = function(name, address) {
        //console.log(name);
        btSerial.findSerialPortChannel(address, function(channel) {
            btSerial.connect(address, channel, function() {
                //console.log('connected');
                deviceInfo.connected = true;
                deviceInfo.name = name;
                io.emit("Device Info", deviceInfo);
     
                /*btSerial.write(Buffer.from('my dataaaaaaaaaaaaaaaaaaaa', 'utf-8'), function(err, bytesWritten) {
                    if (err) console.log(err);
                });*/
     
            }, function () {
                //console.log('cannot connect');
                io.emit("error", "Could Not Connect to Device");
            });
        }, function() {
            //console.log('found nothing');
            io.emit("error", "Make Sure Device is Turned On and Try Again");
        });
    }

    sendString = function(s) {
        btSerial.write(Buffer.from(s), function(err, bytesWritten) {
            if (err) console.log(err);
        });
    }

    btSerial.on('data', function(buffer) {
        parseMessage(buffer);
    });
    
    btSerial.on('found', function(address, name) {
        if(possibleNames.includes(name)) {
            listOfDevices.push({name:name, address:address});
            io.emit("Devices", listOfDevices);
        }
    });
    
    btSerial.on('closed', function() {
        deviceInfo.connected = false;
        deviceInfo.name = "";
        io.emit("Device Info", deviceInfo);
        //console.log("closed");
        setup();
    })
}

setup();
updateDeviceList();